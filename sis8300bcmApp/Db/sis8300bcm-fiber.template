#=================================================================#
# Template file: sis8300bcm-fibr.template
# Database for the records specific to the FIBER block of the 
# individual BCM channel
# Juliano Murari
# July 20, 2022

record(mbbo, "$(P)$(R)OutDataSelect")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.FIBER.OUT_DATA_SELECT")
    field(ZRVL, "0")
    field(ZRST, "ACCT Ch01")
    field(ONVL, "1")
    field(ONST, "ACCT Ch02")
    field(TWVL, "2")
    field(TWST, "ACCT Ch03")
    field(THVL, "3")
    field(THST, "ACCT Ch04")
    field(FRVL, "4")
    field(FRST, "ACCT Ch05")
    field(FVVL, "5")
    field(FVST, "ACCT Ch06")
    field(SXVL, "6")
    field(SXST, "ACCT Ch07")
    field(SVVL, "7")
    field(SVST, "ACCT Ch08")
    field(EIVL, "8")
    field(EIST, "ACCT Ch09")
    field(NIVL, "9")
    field(NIST, "ACCT Ch10")
    field(ASG,  "critical")
    info(autosaveFields, "VAL")
}

record(mbbi, "$(P)$(R)OutDataSelectR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.FIBER.OUT_DATA_SELECT")
    field(SCAN, "I/O Intr")
    field(ZRVL, "0")
    field(ZRST, "ACCT Ch01")
    field(ONVL, "1")
    field(ONST, "ACCT Ch02")
    field(TWVL, "2")
    field(TWST, "ACCT Ch03")
    field(THVL, "3")
    field(THST, "ACCT Ch04")
    field(FRVL, "4")
    field(FRST, "ACCT Ch05")
    field(FVVL, "5")
    field(FVST, "ACCT Ch06")
    field(SXVL, "6")
    field(SXST, "ACCT Ch07")
    field(SVVL, "7")
    field(SVST, "ACCT Ch08")
    field(EIVL, "8")
    field(EIST, "ACCT Ch09")
    field(NIVL, "9")
    field(NIST, "ACCT Ch10")
}

record(bo, "$(P)$(R)OutDataEnable")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.FIBER.OUT_DATA_ENABLE")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
    field(PINI, "YES")
    field(ASG,  "critical")
    info(autosaveFields, "VAL")
}
record(bi, "$(P)$(R)OutDataEnableR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.FIBER.OUT_DATA_ENABLE")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(SCAN, "I/O Intr")
}

record(bi, "$(P)$(R)SFPPresentR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.FIBER.SFP_PRESENT")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

record(bi, "$(P)$(R)LaneUpR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.FIBER.LANE_UP")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

record(bi, "$(P)$(R)ChannelUpR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.FIBER.CHANNEL_UP")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

record(bi, "$(P)$(R)HardwareErrorR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.FIBER.HARDWARE_ERROR")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

record(bi, "$(P)$(R)SoftwareErrorR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.FIBER.SOFTWARE_ERROR")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)Reset")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.FIBER.RESET")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(HIGH, "1")
    field(ASG,  "critical")
}
