
.. cssclass:: table-bordered table-striped table-hover
.. list-table:: LUT common record list
  :widths: 25 50 10 5 10
  :header-rows: 1

  * - Record name
    - Description
    - Record type
    - Access
    - Asyn driver info
  * - $(P)$(R)Lut$(N)MaxPulseLength, $(P)$(R)Lut$(N)MaxPulseLengthR
    - 
    - longout, longin
    - write, read
    - BCM.LUT$(N).MAX_PULSE_LENGTH
  * - $(P)$(R)Lut$(N)LowerThreshold, $(P)$(R)Lut$(N)LowerThresholdR
    - 
    - ao, ai
    - write, read
    - BCM.LUT$(N).LOWER_THRESHOLD
  * - $(P)$(R)Lut$(N)UpperThreshold, $(P)$(R)Lut$(N)UpperThresholdR
    - 
    - ao, ai
    - write, read
    - BCM.LUT$(N).UPPER_THRESHOLD
  * - $(P)$(R)Lut$(N)BeamExists, $(P)$(R)Lut$(N)BeamExistsR
    - 
    - bo, bi
    - write, read
    - BCM.LUT$(N).BEAM_EXISTS
